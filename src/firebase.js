
import { initializeApp } from "firebase/app";
import firebase from "firebase/app";
import "firebase/auth"
import "firebase/firebase-firestore"

const firebaseConfig = {
  apiKey: "AIzaSyD1OBvJxQ8yPtKhh7qKv7X5vcEejdZTvEE",
  authDomain: "krish-3a653.firebaseapp.com",
  projectId: "krish-3a653",
  storageBucket: "krish-3a653.appspot.com",
  messagingSenderId: "433029429730",
  appId: "1:433029429730:web:2cdb3583ecbdced5e161bb"
};

const firebaseApp = firebase.initializeApp(firebaseConfig)
const db = firebase.firestore();
export {db}
