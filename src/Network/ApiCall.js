import { BaseUrl } from "./baseUrl";

const getApicall = async (url, sucessCallback, errorCallback) => {
    await BaseUrl.get(url)
        .then((res) => {
            if (res.status.toString() === '200' || '201') {
                sucessCallback(res)
            }
        })
        .catch((err) => {
            errorCallback(err)
        });
}

const postApicall = async (url, data, sucessCallback, errorCallback) => {
    await BaseUrl.post(url, data)
    .then((res) => {
        console.log("what is res === ", res)
            if (res.status.toString() === '200' || '201') {
                sucessCallback(res.data)
            }
        })
        .catch((err) => {
            errorCallback(err)
        });
}
const puttApicall = async (url, data, sucessCallback, errorCallback) => {
    await BaseUrl.put(url, data)
        .then((res) => {
            if (res.status.toString() === '200' || '201') {
                sucessCallback(res.data)
            }
        })
        .catch((err) => {
            errorCallback(err)
        });
}
const postApicallHead = async (url, data, sucessCallback, errorCallback , header) => {
    await BaseUrl.post(url, data ,header)
    .then((res) => {
        console.log("response kya h ==", res)
            // if (res.status.toString() === '200' || '201') {
                sucessCallback(res.data)
            // }
        })
        .catch((err) => {
            errorCallback(err)
        });
}

export { postApicall, puttApicall, getApicall , postApicallHead }