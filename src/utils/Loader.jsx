import { FallingLines } from 'react-loader-spinner'
const Loader = () => {
    return (
        <div className="Loader" style={{
            height:"70vh",
            display: "flex",
            justifyContent:"center",
            alignItems:"center",
            width:"100%"

        }}>
            <FallingLines
                height="80"
                width="80"
                zIndex="1000"
                radius="9"
                ariaLabel="loading"
                color="#E11D48"
                wrapperStyle
                wrapperClass
            />
        </div>
    )
}

export default Loader