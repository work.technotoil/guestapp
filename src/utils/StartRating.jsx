import React, { useState } from 'react'
import './style.css'
import { Rating } from 'react-simple-star-rating'

export default function StartRating() {
  const [rating, setRating] = useState(0)

  // Catch Rating value
  const handleRating = (rating) => {
    setRating(rating)

    // other logic
  }
  // Optinal callback functions
  const onPointerEnter = () => console.log('Enter')
  const onPointerLeave = () => console.log('Leave')

  return (
    <div className='star_style'>
      <Rating
        onClick={handleRating}
        onPointerEnter={onPointerEnter}
        onPointerLeave={onPointerLeave}
        /* Available Props */
      />
    </div>
  )
}