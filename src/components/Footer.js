import React from "react";
import {
  Box,
  Container,
  Row,
  Column,
  FooterLink,
  Heading,
} from "./FooterStyles";
// import { Document, Page } from 'react-pdf';

const Footer = () => {
  return (
    <Box>
      <h1 style={{ color: "green", textAlign: "center", marginTop: "-50px" }}>
        Kronus.vip: Buy Access. Buy time.
      </h1>
      <Container>
        <Row>
          <Column>
            <Heading>Contact Us</Heading>
            <FooterLink href="#">Address: A-223, DLF The Crest</FooterLink>
            <FooterLink href="#">Gurgaon sector 54</FooterLink>
            <FooterLink href="#">122002</FooterLink>
            <FooterLink href="#">tushar.mehrotra27@gmail.com</FooterLink>
          </Column>
          <Column>
            <Heading href="/privacy">Privacy Policy</Heading>
          </Column>
          <Column>
            <Heading href="/terms">Terms and Conditions</Heading>
          </Column>
          <Column>
            <Heading href="/refund">Refund Policy</Heading>
          </Column>
        </Row>
      </Container>
    </Box>
  );
};
export default Footer;
