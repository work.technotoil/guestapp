import React from 'react'
import { useNavigate } from 'react-router-dom'
import { glassImg } from '../../utils/Images'
import "./style.css"

const ListCom = ({ business_name, ticket , photo }) => {
    
    const Navigate = useNavigate()
    console.log("ticket" , ticket)
    const handleClick = (val , title) => {
        localStorage.setItem("ticketName" , title)
        if (!val.field) {
            Navigate("/ticket-dis", { state: {
                state: {
                    seating: val.seating_price,
                    standing: val.standing_price,
                    seating_at_bar: val.seating_at_bar,
                    standing_at_bar: val.standing_at_bar,
                    title: val.title,
                    icon : val.icon,
                    business_name,
                    photo

                } , 
                value : val.price   
            } })
        } else {

            Navigate("/ticket-type", {
                state: {
                    seating: val.seating_price,
                    standing: val.standing_price,
                    seating_at_bar: val.seating_at_bar,
                    standing_at_bar: val.standing_at_bar,
                    title: val.title,
                    icon : val.icon,
                    business_name,
                    photo

                }
            })
        }

    }
    return (
        <div className="ticket_body">
            {ticket?.map((list, index) => {
                console.log("list" , list)
                const { title, details, price, sub_title, icon , seating_at_bar , standing_at_bar } = list || {};
                return (
                    (seating_at_bar || standing_at_bar) ?
                    <div className="list_show mb-4" key={index}>
                        <div className="ticket">
                            <p className="text-white font-bold text-2xl">{title}</p>
                            <p className="text-white mb-4">{sub_title}</p>
                            {/* <p className="text-white pl-2 insta_color mb-4">{details[0].fourth}</p> */}

                            <p className=" amount_btn bg-white h-8 rounded-lg w-28" onClick={() => handleClick(list , title)}>{price}</p>
                        </div>
                        <div className="image_show">
                            <img src={icon} alt="" />
                        </div>
                    </div> : "" 
                )
            })}
        </div>

    )
}

export default ListCom
