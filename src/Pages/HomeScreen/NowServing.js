import React from "react";
import { BrowserRouter, Link, Route } from "react-router-dom";

export default function NowServing() {
  return (
    <div className="flex justify-center">
      <div className="px-4">
        <img
          className="rounded-lg "
          src="https://m.economictimes.com/thumb/msid-58906902,width-1200,height-900,resizemode-4,imgsize-170486/agood-restaurant-visit-is-a-60-minute-holiday-that-should-satiate-the-soul-saysfoodpreneur-ashish-dev-kapur.jpg"
        />
      </div>

      <div className="object-cover mt-12 w-full h-full flex flex-col justify-center items-center absolute">
        <span className=" text-white text-4xl font-medium ">Whisky Samba</span>
        <p className="text-white text-md text-center font-light p-1">
          One Horizon Center
        </p>
        <p className="text-gray-500 text-sm text-center font-light p-1">
          9:00pm | 🕙 30m
        </p>

        <div className="flex flex-row my-3 gap-2 ">
          <div className="text-gray-500 rounded-full px-2 border border-gray-500">
            Bar
          </div>
          <div className="text-gray-500 rounded-full px-2 border border-gray-500">
            Dine-in
          </div>
          <div className="text-gray-500 rounded-full px-2 border border-gray-500">
            4am
          </div>
        </div>

        <p className="text-gray-500 text-sm text-center font-light px-4 py-2">
          Airy outpost for cocktails, beer & wine alongside a tasting menu with
          elevated gastropub cuisine.{" "}
        </p>

        <Link
          to="/whiskysamba"
          className="bg-rose-600 w-2/3 mt-7 p-2 text-center border border-white  rounded-xl font-bold text-xl shadow-lg text-white"
        >
          <div>
            <button className=" ">Book Now</button>
          </div>
        </Link>
        <Link
          to="tel:8882192787"
          className=" w-2/3 mt-7 p-2 text-center  border-rose-600  rounded-xl font-bold text-xl shadow-lg text-white"
        >
          <div>
            <button className="text-white">Contact Whisky Samba</button>
          </div>
        </Link>
      </div>
    </div>
  );
}
