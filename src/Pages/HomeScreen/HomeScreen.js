import React from "react";
import NowServing from "./NowServing";
import Footer from "../../components/Footer";
import Header from "../../components/Header";

export default function HomeScreen() {
  return (
    <>
    <div className="App flex flex-col items-center">
    <Header />
      <div className="flex mt-12 items-center flex-row h-20 gap-1">
        <div className="text-white bg-rose-600 rounded-full py-2 px-4 ">
          Now Serving
        </div>
        <div className="text-white rounded-full py-2 px-4 ">Coming Soon</div>
      </div>

      <div className="mt-5">
        <NowServing />
      </div>
    </div>
      <Footer />
      </>
  );
}
