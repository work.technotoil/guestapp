import React, { useCallback, useState } from "react";
import { Navigate, Redirect, useNavigate } from "react-router-dom";
import { db } from "../firebase";
import firebase from "firebase";
import Modal from "react-modal";
import QRCode from "react-qr-code";
import axios from "axios";
import { useEffect } from "react";
import Loader from "../components/Loader";
import { BaseUrl, GET_ALL, UPDATE_USER } from "../Network/baseUrl";
import { puttApicall } from "../Network/ApiCall";

// function loadScript(src) {
//   return new Promise((resolve) => {
//     const script = document.createElement("script");
//     script.src = src;
//     script.onload = () => {
//       resolve(true);
//     };
//     script.onerror = () => {
//       resolve(false);
//     };
//     document.body.appendChild(script);
//   });
// }

export default function VipLine({ setObj, obj }) {
  const [total, setTotal] = useState();
  const [payment, setPayment] = useState();
  const [data, setData] = useState({});
  const [userId, setUserId] = useState();
  const [id, setId] = useState("")
  const  [paymentMethod , setPaymentMethod] = useState("")


  useEffect(() => {
    let userId = JSON.parse(localStorage.getItem("userId"));

    setId(userId)
    setUserId(userId);
    let data = JSON.parse(localStorage.getItem("data"));
    setData(data);
  }, []);

  useEffect(() => {
    BaseUrl.get(GET_ALL)
      .then((res) => {
        setTotal(res.data[0].VIP);
        setPayment(res.data[0].VIP);
      })
      .catch((err) => {});
  }, []);
  

  const [quantity, setQuantity] = useState(1);
  const [isActive, setIsActive] = useState(false);
  const [isActive2, setIsActive2] = useState(false);
  const [paymentCard, setPaymentCard] = useState(false);
  const [paymentCash, setPaymentCash] = useState(false);
  const [errorEmpty, setErrorEmpty] = useState("");
  const [loading, setLoading] = useState(false);


  const onPressPlus = () => {
    setQuantity(quantity + 1);
    setTotal(total + payment);
  };

  const onPressMinus = () => {
    setQuantity(Math.max(1, quantity - 1));
    setTotal(Math.max(payment, total - payment));
  };

  const handleClick1 = () => {
    setPaymentMethod("Card")
    setIsActive(true);
    setIsActive2(false);
    setPaymentCard(true);
    setPaymentCash(false);
  };

  const handleClick2 = () => {
    setPaymentMethod("Cash")
    setIsActive2(true);
    setIsActive(false);
    setPaymentCash(true);
    setPaymentCard(false);
  };


  const sucessSignUpCallback =  useCallback(async  (res) => {

    setLoading(false);
    localStorage.setItem("userId", JSON.stringify(res._id));


  });
  
  const errorSignUpCallback =  useCallback(async  (message) => {
    setLoading(false);
  });

  const handleCustomer = async () => {
    // e.preventDefault();
    if (paymentCard || paymentCash) {
      setErrorEmpty("");
      setLoading(true);


      const url = `${UPDATE_USER}${id}`
      const params = {ticketType: obj, amount:total.toString(), quantity: quantity.toString(), paymentType:paymentMethod}
      await puttApicall(url, params, sucessSignUpCallback, errorSignUpCallback)
      // await axios
      //   .put(`http://localhost:3000/api/user/updateUser/${id}`, {
      //     ticketType: obj,
      //     amount: total.toString(),
      //     quantity: quantity.toString(),
      //     paymentType: paymentMethod,
      //   })
      //   .then((res) => {
      //     setLoading(false);
      //     localStorage.setItem("userId", JSON.stringify(res.data._id));
      //   })
      //   .catch((err) => {
      //     setLoading(false);

      //     console.log(err);
      //   });
    } else {
      setErrorEmpty("Please select atleast one payment method.");
    }
    
    // if (paymentCash) {

    //   setLoading(true);
    //   await axios
    //     .put(`http://localhost:3000/api/user/updateUser/${id}`, {
    //       ticketType: obj,
    //       amount: total.toString(),
    //       quantity: quantity.toString(),
    //       paymentType: paymentMethod,
    //     })
    //     .then((res) => {
    //       setLoading(false);
    //       localStorage.setItem("userId", JSON.stringify(res.data._id));
    //       setObj("");
    //     })
    //     .catch((err) => {
    //       setLoading(false);
    //       console.log(err);
    //     });
    // }
    // if (paymentCard === false && paymentCash === false) {
    //   setErrorEmpty("Please select atleast one payment method.");
    // }
  };



  return (
    <div className="App h-screen w-screen flex flex-col items-center justify-center">
      {errorEmpty && (
        <p className="text-center text-red-500 text-xs py-2">{errorEmpty}</p>
      )}

      <div className="flex mt-20 w-screen items-center text-white justify-center font-extrabold text-3xl">
        VIP Fast-Track
      </div>
      <div className="flex w-screen items-center text-white justify-center font-light text-sm pb-10">
        Rs. {payment}/person (100% Redeemable)
      </div>

      <div className="pb-12 flex w-screen  items-center justify-center flex-col">
        <div className="w-screen text-white flex items-center justify-center">
          <input
            value={data.name}
            disabled
            className="px-2 border-rose border w-2/3 h-12 py-1 text-18 bg-black rounded-full text-center"
            placeholder="Name"
          ></input>
        </div>

        <div className="py-4 w-screen text-white flex items-center justify-center">
          <input
            value={data.contact}
            disabled
            className="px-2 border-rose border w-2/3 h-12 py-1 text-18 bg-black rounded-full text-center"
            placeholder="Phone Number"
          ></input>
        </div>

        <div className="flex flex-row-reverse mt-5">
          <div
            className="font-bold text-white text-xl p-5"
            onClick={onPressPlus}
          >
            +
          </div>
          <div className="font-bold text-white text-xl p-5">{quantity}</div>
          <div
            className="font-bold text-white text-lg p-5"
            onClick={onPressMinus}
          >
            -
          </div>
        </div>

        <div className="font-bold text-white mt-24 text-lg">
          Select a payment method
        </div>

        <div className="flex pt-4 gap-4 text-md ">
          <div
            style={{
              background: isActive ? "white" : "transparent",
              color: isActive ? "black" : "white",
            }}
            onClick={handleClick1}
            className="bg-black text-white p-4 rounded-lg"
          >
            CARD
          </div>
          <div
            style={{
              background: isActive2 ? "white" : "transparent",
              color: isActive2 ? "black" : "white",
            }}
            onClick={handleClick2}
            className="bg-black text-white p-4 rounded-lg"
          >
            CASH
          </div>
        </div>
      </div>

      <div className="flex w-screen place-content-between  flex-row px-10 items-center h-20 ">
        <div className="flex flex-col">
          <div className="text-white font-light text-lg">Total Price</div>
          <div className="font-bold text-white text-3xl">Rs. {total}</div>
        </div>
        <div>
          <button
            onClick={handleCustomer}
            className="p-2 hover:font bold hover:text-black hover:bg-white w-32 bg-rose-600 text-xl text-white rounded-full shadow-lg mb-48"
            disabled={loading}
          >
            {loading ? <Loader /> : "Book Now"}
          </button>
        </div>
      </div>


    </div>
  );
}


