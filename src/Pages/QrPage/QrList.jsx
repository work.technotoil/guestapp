import React from 'react'
import { Imageurl } from '../../Network/baseUrl'

const QrList = ({onClick , item}) => {
    const {business_name ,date, price , photo , } = item || {}
    const MainDate = date?.slice(0 , 10)
    const MainPhoto = photo.replace("uploads\\" , "")
    return (
        <div className='qr_list' onClick={onClick}>
            <div className="qr_bar_img">
                <img src={`${Imageurl}/${MainPhoto}`} alt="" />
                {/* <img src={MainPhoto} alt="" /> */}
                <div className="bar_img_desc">
                    <h4 className='bar_name'> {business_name}</h4>
                    <p className='bar_date'>{MainDate}</p>
                </div>
            </div>
            <div className="qr_price">
                <h4 className='qr_price_p'>{price}</h4>
            </div>
        </div>
    )
}

export default QrList