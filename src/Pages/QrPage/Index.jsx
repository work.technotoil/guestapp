import React from 'react'
import { useState } from 'react'
import { useEffect } from 'react'
import {  useNavigate } from 'react-router-dom'
import Footer from '../../components/Footers'
import { BaseUrl } from '../../Network/baseUrl'
import { SearchIcon } from '../../utils/Images'
import Loader from '../../utils/Loader'
import "./Index.css"
import QrList from './QrList'

const QrPage = () => {
    const [ApiResponse , setApiResponse] = useState([])
    const [loading , setLoading] = useState(false)
    const Navigate = useNavigate()
    const onClick = (item)=>{
        console.log(item)
        Navigate("privee-page" , {state:item })
    }
    const UserId = localStorage.getItem("userId")

    console.log("mian id"   , UserId)
    const ApiExtract = async()=>{
        try {
            setLoading(true)
            const {data} = await BaseUrl.get(`getBooking/${UserId}`)
            // const {data} = await BaseUrl.get(`http://localhost:8000/api/getBooking/63418cb345092efcc5d196ab`)
            if(data.success){
                setApiResponse(data.data)
            } 
        } catch (error) {
            console.log(error)
        }finally{
            setLoading(false)
        }
        
    }

    useEffect(()=>{
        ApiExtract()
    },[])
    return (
        <div className='qr_page'>
            
            <div className="Qrscreens">
            
                <div className="transactionList">
                    
                    {loading ? <Loader /> : <>
                    
                    <div className="barLine">
                        <hr />
                    </div>
                    <div className="transactionHead">
                        
                        <h3> Transaction</h3>
                        {/* <img src={SearchIcon} alt="" />
                        <input type="text" placeholder='Search'/> */}
                    </div>
                    <div className="SearchLine">
                        <hr />
                    </div>
                    <div className="bar_qr_list">
                        
                        {ApiResponse.map((item , index)=>{
                            return(
                                <QrList key={index} item={item} onClick={()=>onClick(item)}/>
                            )
                        })}
                     
                       
                    </div> </> }

                </div> 
            </div> 
            <Footer />
        </div>
    )
}

export default QrPage