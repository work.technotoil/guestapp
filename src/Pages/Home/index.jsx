import React, { useCallback, useEffect, useState } from 'react'
import "./style.css"
import Footer from '../../components/Footers/index'
import TopHeader from '../../components/TopHeader'
import { phoneIcon, topicIcon } from '../../utils/Images'
import ListCom from '../../components/ListCom/ListCom'
import { useNavigate, useParams } from 'react-router-dom'
import { getApicall } from '../../Network/ApiCall'
import { BUSINESS } from '../../Network/baseUrl'

const Home = () => {
  const [loading, setLoading] = useState(false)
  const [getData, setGetData] = useState({})
  const { id } = useParams()
  const navigate = useNavigate()
  const { business_name, ticket, photo } = getData || {};
  let userData = JSON.parse(localStorage.getItem("data"))
  const backClick = () => {
    // navigate(`/whisky-samba/${id}`)
    navigate(-1)
  }
  const Number = localStorage.getItem("number")


  const sucessSignUpCallback = useCallback((response) => {
    setGetData(response.data)
    console.log(response.data)
    setLoading(false);
  });

  const errorSignUpCallback = useCallback((message) => {
    setLoading(false);
  });

  const barData = () => {
    setLoading(true)
    const url = `${BUSINESS}${id}`
    getApicall(url, sucessSignUpCallback, errorSignUpCallback)
  }
  useEffect(() => {
    barData();
  }, [])

  return (
    <div className="flex flex-col items-center justify-center py-4" style={{marginTop:"43px"}}>
      <TopHeader username={userData.name} backClick={backClick} />
      <div className="msg_body">
        <div className="">
          {business_name}
        </div>
        <div className="top_phone">
          <img src={topicIcon} alt="" />
          <a href={`tel:${Number}`}>   <img src={phoneIcon} alt="" /></a>
        </div>
      </div>
      <ListCom ticket={ticket} business_name={business_name} photo={photo} />
      <Footer />
    </div>
  )
}

export default Home