import React from 'react'

export default function Clients2(x) {
  return (
    <div className="py-1 px-3">
        <div className="items-center text-center flex place-content-between rounded-lg px-2 py-1 border-black border">
        <div className="">{x.guestNum2} person(s)</div>
            <div className="basis-1/6 font-bold text-800 ">{x.identity2}</div>   
            <div className="basis-1/6 ">{x.paymentMethod2}</div>
            <div className="basis-1/6 font-bold">{x.amount2}</div>
            <div className="basis-1/6 ">{x.lead2}</div>
            <div className="basis-1/6 ">{new Date(x.timestamp2?.toDate()).toLocaleString()}</div>
            <div className="basis-1/6 p-4">{x.Id}</div>
        </div>
    </div>
  )
}
