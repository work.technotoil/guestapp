import React, {useState, useEffect} from 'react'
import {useDispatch, useSelector} from 'react-redux';
import {setUserInfo} from '../../../features/userSlice';
import {db} from '../../../firebase'
import firebase from 'firebase';


export default function Clients(x) {

  const handleClient = () => {
    firebase.firestore().collection("2500").doc(userId).get().then((snapshot) => {
      console.log(snapshot.data())
    }).catch((e) => console.log(e))
  }

  const [realStatus, setRealStatus] = useState('')
  const dispatch = useDispatch();

  const userId = useSelector((state) => state.user.userId);
  const userName = useSelector((state) => state.user.userName);


  const handlePaymentConfirmation_YES = (e)=>{
    e.preventDefault();
    
    if(userId){
      db.collection("2500").doc(userId).collection('paymentStatusCheck').add({
        paymentMadeCheck: "YES",
        paymentMadeTime: firebase.firestore.FieldValue.serverTimestamp(),
      })
    }
    setRealStatus("PAID")

  }

  const handlePaymentConfirmation_NO = (e)=>{
    e.preventDefault();
    
    if(userId){
      db.collection("2500").doc(userId).collection('paymentStatusCheck').add({
        paymentMadeCheck: "NO"
      })
    }
    setRealStatus("UNPAID")

  }

  let showBar; 

  if(realStatus === ""){
    showBar = 
    <div className="flex gap-4 font-bold">
      <div  className="bg-green-500 px-2 py-1 rounded-lg"> <button onClick={handlePaymentConfirmation_YES}>YES</button></div>
       <div className="bg-red-500 px-2 py-1 rounded-lg"><button onClick={handlePaymentConfirmation_NO}>NO</button></div>
    </div>
  } else if(realStatus === "PAID"){
    showBar = 
    <div>
      <div className="font-bold">PAID</div>
    </div>
  } else if(realStatus === "UNPAID"){
    showBar = 
    <div className="font-bold">UNPAID</div>
  }


  return (
    <div className="py-1 px-3"
    onClick={()=>
      dispatch(setUserInfo({
        userId: x.Id,
        userName: x.identity,
      }))}
      
    >
      
        <div className="items-center text-center flex place-content-between rounded-lg px-2 py-1 border-black border">
            <div className="">{x.guestNum} person(s)</div>
            <div className="basis-1/6 font-bold text-800 ">{x.identity}</div>   
            <div className="basis-1/6 ">{x.paymentMethod}</div>
            <div className="basis-1/6 font-bold">{x.amount}</div>
            <div className="basis-1/6 ">{x.lead}</div>
            <div className="basis-1/6 ">{new Date(x.timestamp?.toDate()).toLocaleString()}</div>
            <div className="basis-1/6 p-4">{x.Id}</div>
            
            <div className="">
              {showBar}
            </div>
        </div>

        {/* <button classNameName="text-white bg-black p-6" onClick={handleClient}>HEllo</button> */}
      
    </div>
  )
}
