import React from 'react'

export default function Privacy() {
  return (
    <div className="App w-screen flex flex-col items-center py-10">

        <p className="text-white text-sm w-5/6 mt-5">Effective on August 1st, 2022</p>

        <div className='text-white font-bold text-4xl'>Privacy Policy</div>
        <p className="text-white text-sm w-5/6 mt-5"></p>

        <div className='text-white font-bold text-4xl'>Introduction & Scope</div>
        <p className="text-white text-sm w-5/6 mt-5">(“Dream Canteen Pvt Ltd.”, “we”, “us”, “our”) takes the protection of personally identifiable information (“Personal Data”) very seriously. This Privacy Policy (the “Policy”) addresses data subjects whose Personal Data we may receive from our customers on our website [Insert Web Address] (collectively, the “Services”). </p>
        
        <div className='text-white font-bold text-4xl'>Controllership</div>
        <p className="text-white text-sm w-5/6 mt-5">Dream Canteen Pvt Ltd.] acts as an agent, also known as a data processor, for the Personal Data we process for our customers when providing our Services. This means that our customers determine the type of Personal Data they provide for us to process on their behalf. We typically have no direct relationship with the individuals whose Personal Data we receive from our customers.</p>

        <div className='text-white font-bold text-4xl'>Basis of Processing</div>
        <p className="text-white text-sm w-5/6 mt-5">We may process your Personal Data on the basis of:
●	your consent;
●	the need to perform a contract with you;
●	our legitimate interests or those of a third party, such as our interest in marketing our Services;
●	the need to comply with the law; or
●	any other ground, as required or permitted by law.
</p>

        <div className='text-white font-bold text-4xl'>How we recieve personal data</div>
        <p className="text-white text-sm w-5/6 mt-5">We may receive your Personal Data when:
●	you provide it directly to us as part of using our Services;
●	our customers (including their employees, contractors, and other representatives of the company) provide it to us;
●	our service providers provide it to us.
</p>

        <div className='text-white font-bold text-4xl'>Categories of personal data</div>
        <p className="text-white text-sm w-5/6 mt-5">We may process the following types of Personal Data: [THE FOLLOWING SHOULD BE EXPANDED OR ADJUSTED DEPENDING ON SCOPE OF DATA COLLECTED]
●	biographical information, such as Dream Canteen Pvt Ltd. or first and last name in the case of a person; and
●	contact information, such as email address and postal addresses.
</p>

        <div className='text-white font-bold text-4xl'>Purposes of processing</div>
        <p className="text-white text-sm w-5/6 mt-5">We may process your Personal Data for the purposes of:
●	enabling the use of the Services;
●	responding to your requests or questions; and
●	sending you email marketing communications about our business which we think may interest you.
</p>

        <div className='text-white font-bold text-4xl'>Data retention</div>
        <p className="text-white text-sm w-5/6 mt-5">When the purposes of processing are satisfied, we will delete the related Personal Data within one year.</p>

        <div className='text-white font-bold text-4xl'>Sharing personal data with third parties</div>
        <p className="text-white text-sm w-5/6 mt-5">We may share Personal Data with our service providers, who process Personal Data on our behalf, and who agree to use the Personal Data only to assist us in providing our Services or as required by law. Our service providers may provide: [THE FOLLOWING SHOULD BE EXPANDED OR ADJUSTED DEPENDING ON SCOPE OF SERVICES BEING USED]
●	application hosting services;
●	cloud storage services;
●	email software; 
●	advertising and marketing services; and
●	CRM software.
</p>

        <div className='text-white font-bold text-4xl'>Other disclosure of your personla data</div>
        <p className="text-white text-sm w-5/6 mt-5">We may disclose your Personal Data to the extent required by law, or if we have a good-faith belief that we need to disclose it in order to comply with official investigations or legal proceedings (whether initiated by governmental/law enforcement officials, or private parties). We may also disclose your Personal Data if we sell or transfer all or some of our company’s business interests, assets, or both, or in connection with a corporate restructuring. Finally, we may disclose your Personal Data to our subsidiaries or affiliates, but only if necessary for business purposes, as described in the section above.

We reserve the right to use, transfer, sell, and share aggregated, anonymous data, (which does not include any Personal Data) about individuals whose Personal Data we process in connection with providing our Services, for any legal business purpose. These purposes may include analyzing usage trends or seeking compatible advertisers, sponsors, and customers.

If we have to disclose your Personal Data to governmental/law enforcement officials, we may not be able to ensure that those officials will maintain the privacy and security of your Personal Data.
</p>

        <div className='text-white font-bold text-4xl'>Cookies</div>
        <p className="text-white text-sm w-5/6 mt-5">A “cookie” is a small file stored on your device that contains information about your device. We may use cookies to provide basic relevant ads, website functionality, authentication (session management), usage analytics (web analytics), and to remember your settings, and generally improve our websites and Services. [THE FOLLOWING SHOULD BE EXPANDED OR ADJUSTED DEPENDING ON TYPES OF COOKIES OR TRACKING BEING USED-THIS IS RELATIVELY STANDARD FOR MOST WEBSITES]

We use session and persistent cookies. Session cookies are deleted when you close your browser. Persistent cookies may remain even after you close your browser, but always have an expiration date. Most of the cookies placed on your device through our Services are first-party cookies, since they are placed directly by us. Other parties, such as Google, may also set their own (third-party) cookies through our Services. Please refer to the policies of these third parties to learn more about the way in which they collect and process information about you.

If you would prefer not to accept cookies, you can change the setup of your browser to reject all or some cookies. Note, if you reject certain cookies, you may not be able to use all of our Services’ features. For more information, please visit https://www.aboutcookies.org/.

You may also set your browser to send a Do Not Track (DNT) signal. For more information, please visit https://allaboutdnt.com/. Please note that our Services do not have the capability to respond to “Do Not Track” signals received from web browsers.
</p>

        <div className='text-white font-bold text-4xl'>Data Integrity & Security</div>
        <p className="text-white text-sm w-5/6 mt-5">We have implemented and will maintain technical, administrative, and physical measures that are reasonably designed to help protect Personal Data from unauthorized processing. This includes unauthorized access, disclosure, alteration, or destruction.</p>

        <div className='text-white font-bold text-4xl'>Access & Review</div>
        <p className="text-white text-sm w-5/6 mt-5">If we process your Personal Data, you may have the right to request access to (or to update, correct, or delete) such Personal Data. You may also have the right to ask that we limit our processing of such Personal Data, as well as the right to object to our processing of such Personal Data. You may also have the right to data portability.</p>

        <div className='text-white font-bold text-4xl'>Privacy of children</div>
        <p className="text-white text-sm w-5/6 mt-5">The Services are not directed at, or intended for use by, children under the age of 13.</p>

        <div className='text-white font-bold text-4xl'>Changes to this poilicy</div>
        <p className="text-white text-sm w-5/6 mt-5">If we make any material change to this Policy, we will post the revised Policy to this web page. We will also update the “Effective” date. By continuing to use our Services after we post any of these changes, you accept the modified Policy.</p>

        <div className='text-white font-bold text-4xl'>Contact us</div>
        <p className="text-white text-sm w-5/6 mt-5">If you have any questions about this Policy or our processing of your Personal Data, please write by email at prathamshankar3@gmail.com.</p>



        

    </div>
  )
}
