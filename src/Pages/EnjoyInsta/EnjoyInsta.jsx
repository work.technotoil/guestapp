import React from 'react'
import { useNavigate } from 'react-router-dom'

import Button from '../../components/Button/Button'

const EnjoyInsta = () => {
    const navigate = useNavigate()
    const handleClick =()=>{
      navigate("/confirm-your-detail")
    }
  return (
    <div className=" h-screen w-screen flex flex-col items-center py-10" >
      <div className=" text-4xl text-center mb-4 px-4">Enjoy Instant Access Anywhere</div>
      <div className="mb-44 font-bold">
        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZP6pAvRf6nrDm8TzUu-J6WniYKqGWPnXVnGeqefiExHZfTiX7N9SSRLiwyqB6z97sDB8&usqp=CAU" alt="" />
      </div>

      <div>
        <Button btnText="Got it!..." 
        handleClick={handleClick}
        className="p-2  hover:font bold hover:text-black hover:bg-white bg-rose-600 text-white border border-black w-60 text-white text-base rounded-full mb-2" />
      </div>
    </div>
  )
}

export default EnjoyInsta